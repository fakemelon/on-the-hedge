﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitch : MonoBehaviour
{
    [SerializeField]
    private Transform player;

    private Vector3 overviewPosition;
    private Quaternion overviewRotation;
    private Camera camera;

    private void Awake()
    {
        camera = GetComponent<Camera>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (transform.IsChildOf(player))
            {
                transform.parent = null;
                transform.position = overviewPosition;
                transform.rotation = overviewRotation;
                camera.orthographic = true;
            }
            else
            {
                //camera.si = 9;
                overviewPosition = transform.position;
                overviewRotation = transform.rotation;
                transform.SetParent(player);
                transform.position = player.position;
                transform.rotation = player.rotation;
                camera.orthographic = false;
            }
        }
    }

    public void SetPlayerPosition(Vector3 position)
    {
        player.position = position;
    }
}

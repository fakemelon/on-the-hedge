﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Field : MonoBehaviour
{
    [SerializeField]
    private int radius;
    [SerializeField]
    private Tile tilePrefab;
    [SerializeField]
    private GameObject wallPrefab;
    [SerializeField]
    private GameObject diamondPrefab;

    private Tile[,] tiles;

    public Tile[,] Tiles { get => tiles; }

    private void Awake()
    {
        tiles = new Tile[radius * 2 + 1, radius];
    }

    private void Start()
    {
        CreateField();
        CreateWalls();

        Vector3 middleTilePosition = tiles[tiles.GetLength(0) / 2, tiles.GetLength(1) / 2].transform.position;
        Instantiate(diamondPrefab, middleTilePosition+new Vector3(0,1,0), Quaternion.identity, transform);
        Camera.main.transform.position = new Vector3(middleTilePosition.x, radius, middleTilePosition.z - 0.5f);
        Camera.main.transform.Rotate(90, 0, 0);
        Camera.main.orthographic = true;
        Camera.main.orthographicSize = radius / 2;
    }

    private void CreateField()
    {
        for (int i = -1; i < tiles.GetLength(0)+1; i++)
        {
            for (int j = -1; j < tiles.GetLength(1)+1; j++)
            {
                if(j!=-1&&i!=-1 && j != tiles.GetLength(1) && i != tiles.GetLength(0))
                {
                    tiles[i, j] = Instantiate(tilePrefab, new Vector3(i, 0, j), Quaternion.identity, transform);
                    tiles[i, j].SetPositionOnField(i, j);
                }
                else
                {
                    Instantiate(wallPrefab, new Vector3(i, 1, j), Quaternion.identity, transform);
                }

            }
        }
    }

    private void CreateWalls()
    {
        
    }
}

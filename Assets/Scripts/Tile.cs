﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    private int x;
    private int y;

    public int X { get => x; }
    public int Y { get => y; }

    public void SetPositionOnField(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
}

﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Painter : MonoBehaviour
{
    [SerializeField]
    private CameraSwitch cameraSwitch;
    [SerializeField]
    private Field field;
    [SerializeField]
    private Block[] Blocks;
    [SerializeField]
    private int[] blocksLeft;
    private BlockTypes chosenBlock = BlockTypes.HEDGE;
    [SerializeField]
    private Text[] blockButtonTexts;

    private void Start()
    {
        UpdateButtons();
    }

    private void UpdateButtons()
    {
        for (int i = 0; i < blockButtonTexts.Length; i++)
        {
          string TypeText;
          switch (i)
          {
                case (int)BlockTypes.HEDGE:
                    TypeText = "HEDGE";
                    break;
                case (int)BlockTypes.WALL:
                    TypeText = "WALL";
                    break;
                case (int)BlockTypes.GLASS:
                    TypeText = "GLASS";
                    break;
                case (int)BlockTypes.MUD:
                    TypeText = "MUD";
                    break;
                case (int)BlockTypes.RIVAL:
                    TypeText = "RIVAL";
                    break;
                default:
                    TypeText = "lolelelolololoolkekekeke";
                    break;
          }
          blockButtonTexts[i].GetComponent<Text>().text = 
                TypeText + " X" + blocksLeft[i];
        }
    }
    public void ChooseBlockType(int Type)
    {
        chosenBlock = (BlockTypes)Type;
    }

    private void Update()
    {
        if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if (blocksLeft[(int)chosenBlock] > 0)
                {
                    GameObject hitGameObject = hit.collider.gameObject;
                    if (hitGameObject.GetComponent<Tile>() != null &&
                    IsPositionValid(hitGameObject.GetComponent<Tile>()))
                    {
                        BuildNewBlock(hit);
                        if (chosenBlock == BlockTypes.RIVAL)
                        {
                            cameraSwitch.SetPlayerPosition(hitGameObject.transform.position + Vector3.up);
                        }
                    }
                    else if (hitGameObject.GetComponent<Block>() != null && chosenBlock != BlockTypes.MUD /*IsPositionValid(hit.collider.gameObject.GetComponent<Tile>()) &&*/)
                    {
                        if (hitGameObject.GetComponent<Block>().Type == BlockTypes.MUD)
                        {
                            BuildNewBlock(hit);
                        }
                    }
                }
               
            }
        }

        if (Input.GetMouseButton(1))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject.GetComponent<Block>() != null)
                {  
                    blocksLeft[(int)hit.collider.gameObject.GetComponent<Block>().Type]++;
                    Destroy(hit.collider.gameObject);
                    UpdateButtons();
                }
            }
        }
    }
    private void BuildNewBlock(RaycastHit hit)
    {
        Block newBlock = Instantiate(Blocks[(int)chosenBlock],
            new Vector3 (hit.transform.position.x, 0.5f + (Blocks[(int)chosenBlock].transform.localScale.y / 2f), hit.transform.position.z),
            Quaternion.identity, hit.transform.parent);
        //newBlock.gameObject.transform.position.y += newBlock.transform.localScale.y;
        newBlock.Type = chosenBlock;
        blocksLeft[(int)chosenBlock]--;
        UpdateButtons();
    }
    private bool IsPositionValid(Tile tile)
    {
        bool isValidOnX = false;
        for (int i = 0; i < field.Tiles.GetLength(0); i++)
        {
            if (i != tile.X)
            {
                if (field.Tiles[i, tile.Y].transform.childCount == 0)
                {
                    isValidOnX = true;
                    break;
                }
            }
        }

        bool isValidOnY = false;
        for (int i = 0; i < field.Tiles.GetLength(1); i++)
        {
          if (i != tile.Y)
          {
              if (field.Tiles[tile.X, i].transform.childCount == 0)
              {
                    isValidOnY = true;
                    break;
              }
          }
        }
        return isValidOnX && isValidOnY;
    }
}
/*private GameObject[] hedgePrefab;
[SerializeField]*/
/* [SerializeField]
 private GameObject pickedBlockType= hedgePrefab;*/

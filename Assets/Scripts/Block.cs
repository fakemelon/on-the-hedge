﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BlockTypes { HEDGE, WALL, GLASS, MUD, RIVAL };
public class Block : MonoBehaviour
{
    private BlockTypes type;
    public BlockTypes Type { get => type; set => type = value; }
}
